<?php

/* 
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Автоматически подключает классы. Сначала ищет в каталоге
 * orion (каталог с фреймворком), далее в каталоге controllers 
 * и каталоге models.
 * 
 * @author Maksim Ivanov (dark_orion) <minotaurmax@hotmail.com>
*/

function class_autoload($class_name)
{
    $file = ORION . 'classes/' . ucfirst(strtolower($class_name)) . '.php';
    if (file_exists($file) == false) {
        return false;
    }
    require_once ($file);
    return true;
}

function controller_autoload($class_name)
{
    $file = APP . 'controllers/' . preg_replace('#controller$#i', 'Controller',
            ucfirst(strtolower($class_name))) . '.php';
    if (file_exists($file) == false) {
        return false;
    }
    require_once ($file);
    return true;
}

function model_autoload($class_name)
{
    $file = APP . 'models/' . ucfirst(strtolower($class_name)) . '.php';
    if (file_exists($file) == false) {
        return false;
    }
    require_once ($file);
    return true;
}

spl_autoload_register('class_autoload');
spl_autoload_register('controller_autoload');
spl_autoload_register('model_autoload');
