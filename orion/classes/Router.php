<?php
/* 
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Парсит url получая имена контроллера, action и id
 * 
 * @author Maksim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */

class Router extends Singleton
{

    private $path_elements = array('controller', 'action', 'id');

    // парсинг url
    function parse($path)
    {
        $request = $_REQUEST;

        $request['controller'] = app::gi()->config->default_controller;
        $request['action'] = app::gi()->config->default_action;
        $request['id'] = 0;
        $parts = parse_url($path);
        if (isset($parts['query']) and !empty($parts['query'])) {
            $path = str_replace('?' . $parts['query'], '', $path);
            parse_str($parts['query'], $req);
            $request = array_merge($request, $req);
        }
        foreach (app::gi()->config->router as $rule => $keypath) {
            if (preg_match('#' . $rule . '#sui', $path, $list)) {
                for ($i = 1; $i < count($list); $i = $i + 1) {
                    $keypath = preg_replace('#\$[a-z0-9]+#', $list[$i], $keypath, 1);
                }
                $keypath = explode('/', $keypath);
                foreach ($keypath as $i => $key) {
                    $request[$this->path_elements[$i]] = $key;
                }
            }
        }

        return $request;
    }
}
