<?php

/*
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Модель для работы с БД.
 * 
 * @author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */
abstract class ModelTable extends Model
{

    public $errors = array();
    static public $table = '{table}';
    static public $primary = 'id';

    function beforeSave()
    {
        return !count($this->errors);
    }

    /*
     * Сохраняет модель в базу данных. Если строка с таким id существует, то
     * то запись обновляется, если не существует, то добавляется. 
     */
    function save()
    {
        $modelname = get_called_class();
        if ($this->beforeSave()) {
            $data = (array)$this->_data;
            if (!$this->__get(self::$primary)) {              
                $res = App::gi()->db->query('INSERT INTO ?n SET ?u',
                        $modelname::$table, $data);
                $this->__set($modelname::$primary, App::gi()->db->insertId());
                return $res;
            } else {
                return App::gi()->db->query('UPDATE ?n SET ?u WHERE ' . $modelname::$primary . '= ?i',
                        $modelname::$table, $data, $this->__get(self::$primary));
            }
        }
    }

    /*
     * Запрос к базе данных.
     */
    static function getQuery()
    {
        $modelname = get_called_class();
        return App::gi()->db->query('SELECT * FROM ?n ', $modelname::$table);
    }

    /*
     * Извлекает записи из базы
     */
    static function models()
    {
        $items = static::getQuery();
        $results = array();
        $modelname = get_called_class();
        foreach ($items as $item) {
            $model = new $modelname();
            $model->__attributes = $item;
            $results[] = $model;
        }
        return $results;
    }

    /*
     * Извлекает запись из базы.
     */
    static function model($id)
    {
        $modelname = get_called_class();
        $item = App::gi()->db->getRow('SELECT * FROM ?n WHERE ' 
                . $modelname::$primary . '= ?i', $modelname::$table, $id);
        $model = new $modelname();
        $model->__attributes = $item;
        return $model;
    }
    
    /*
     * Удаление записи
     */
    static function delete($id) {
        $modelname = get_called_class();
        return App::gi()->db->query("DELETE FROM ?n WHERE id=?i", $modelname::$table, $id);
    }

}
