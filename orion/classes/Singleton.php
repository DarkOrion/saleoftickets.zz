<?php
/* 
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Метод gi() возвращает единственный экземпляр класса. Можно наследовать
 * данный класс.
 * 
 * @author Maksim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */

abstract class Singleton
{

    private static $_aInstances = array();

    public static function getInstance($className = false)
    {
        $sClassName = ($className === false) ? get_called_class() : $className;

        if (class_exists($sClassName)) {
            if (!isset(self::$_aInstances[$sClassName])) {
                self::$_aInstances[$sClassName] = new $sClassName();
            }
            $oInstance = self::$_aInstances[$sClassName];
            return $oInstance;
        } else {
            throw new Except('Class ' . $sClassName . '  no exist!');
        }
    }

    public static function gI($className = false)
    {
        return self::getInstance($className);
    }

    final private function __clone() {}

    private function __construct() {}
}
