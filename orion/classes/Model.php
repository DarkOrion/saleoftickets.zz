<?php

/* 
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Базовый класс моделлей.
 * 
 * @author Maksim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */
class Model
{
    /*
     * Данные модели
     */
    protected $_data = null;
    
    /*
     * Доступные данные для сохранения. Поле должно переопределятся
     * в классе наследнике
     */
    public $safe = [];

    function __construct()
    {
        $this->_data = new stdClass();
    }
    
    /*
     * Для вставки значений. Если имя переданной переменной name - __attributes,
     * то значение воспринимается как ассоциативный массив и вставляются поэле-
     * ментно. Если есть метод с соответствующим именем, то вызывается этот ме-
     * тод и в параметрах передается value.
     */
    function __set($name, $value)
    {
        if ($name === '__attributes') {
            foreach ($value as $key => $val) {
                $this->__set($key, $val);
            }
            return;
        }
        if (method_exists($this, 'set' . $name)) {
            return call_user_func(array($this, 'set' . $name), $value);
        }
        if (in_array($name, $this->safe)) {
            $this->_data->$name = $value;
        }
    }

    /*
     * Для извлечения значений. Если имя переданной переменной name - __attributes,
     * то возвращается объект _data Если есть метод с соответствующим именем, 
     * то вызывается этот метод.
     */
    function __get($name)
    {
        if ($name === '__attributes') {
            return $this->_data;
        }
        if (method_exists($this, 'get' . $name)) {
            return call_user_func(array($this, 'get' . $name));
        }
        return property_exists($this->_data, $name) ? $this->_data->$name : null;
    }
    
    /*
     * Фильтр для запроса
     */ 
    static function filter($value = []) {
        $filter_val = [];
        foreach ($value as $key => $val) {
            $filter_val[$key] = trim(filter_var($val, FILTER_SANITIZE_STRING));
        }
        return $filter_val;
    }
}
