<?php

/* 
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Настройки сайта, используются по умолчанию.
 * 
 * @author Maksim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */

return ['sitename' => 'Orion',
    'encode' => 'utf-8',
    'cookietime' => 3600, 
    'version' => '',
    'default_controller' => 'index',
    'default_action' => 'index',
    'db' => array(),
    
    // поиск контроллера и модели по url

    'scripts' => array(
        '/assets/js/libs/jquery-2.1.3.min.js',
        '/assets/js/libs/bootstrap/js/bootstrap.min.js',
    ),
    'styles' => array(
        '/assets/js/libs/bootstrap/css/bootstrap.min.css',
        '/assets/js/libs/bootstrap/css/bootstrap-theme.min.css',
    ),
];
