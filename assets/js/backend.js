/* 
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$(document).ready(function () {

    $('#datepicker').datetimepicker({
        timepicker: false,
        format: "Y-m-d",
    });
    
    $('#datetimepicker1').datetimepicker({
        format: "Y-m-d H:i",
    });
    
    $('#datetimepicker2').datetimepicker({
        format: "Y-m-d H:i",
    });

    var queryResult = null;

    $("#createCli").click(function(e) {
        if ($("#idPers").val() == 0) {
            postForm(e, "/person/create/aj", "#personform");
            $("#resultPers").html(queryResult);
            $("input:text").each(function(nf, inputData)
            {
                $(inputData).val("");
            });
        }
    });
    
    $("#createFli").click(function(e) {
        if ($("#idFli").val() == 0) {
            postForm(e, "/flight/create/aj", "#flightform");
            $("#resultFli").html(queryResult);
            $("input:text").each(function(nf, inputData)
            {
                $(inputData).val("");
            });
        }
    });

    function postForm(e, path, formId) {
        e.preventDefault();
        var data = $(formId).serialize();
        $.ajax({
            type: "POST",
            url: path,
            data: data,
            async: false,
            success: function (msg) {
                queryResult = msg;
            }
        });
    }
});


