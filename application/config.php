<?php
/* 
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Пользовательские настройки сайта.
 * 
 * @author Maksim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */

return [
    'sitename' => 'Продажа авиабилетов',
    'db' => include 'config.db.php',
    'router' => array(
        '([a-z0-9+_\-]+)/([a-z0-9+_\-]+)/([0-9]+)/(aj)' => '$controller/$action/$id/$aj',
        '([a-z0-9+_\-]+)/([a-z0-9+_\-]+)/(aj)' => '$controller/$action/$aj',
        '([a-z0-9+_\-]+)/([a-z0-9+_\-]+)/([0-9]+)' => '$controller/$action/$id',
        '([a-z0-9+_\-]+)/(aj)' => '$controller/$aj',
        '([a-z0-9+_\-]+)/([a-z0-9+_\-]+)' => '$controller/$action',
        '([a-z0-9+_\-]+)/?' => '$controller',
        '([a-z0-9+_\-]+)\.html' => 'page/read/$id',   
    ),
];
