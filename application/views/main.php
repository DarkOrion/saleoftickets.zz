<!DOCTYPE html>

<!--
 Copyright (C) 2015 Maxim Ivanov (dark_orion)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!--
 @author Maksim Ivanov (dark_orion) <minotaurmax@hotmail.com>
-->

<html lang="ru">
  <head>
    <meta charset="<?= app::gi()->config->encode ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?= app::gi()->config->sitename ?></title>
    <link rel="icon" href="/assets/images/favicon.ico">
    <!--[if lt IE 9]> 
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
    <![endif]-->
  </head>

  <body>

    <div class="container">
      <?php $this->addStyleSheet('/assets/css/main.css', 'body'); ?>
      <?php include dirname(__FILE__) . '/layouts/' . $this->layout . '.php'; ?>
    </div><!-- /.container -->  

  </body>
</html>
