<!--
Copyright (C) 2015 Maxim Ivanov (dark_orion)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!--

@author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
-->
<div class="row">
  <h1>Клиенты и пассажиры</h1>
</div>
<a href="/person/index" class="btn btn-info btn-md">Список персон</a>
<div class="row col-md-12 center-block form-row">
  <h2><?=$item->id ? 'Изменить данные' : 'Добавить персону'?></h2> 
  <form class="form-horizontal" method="post" id="personform">
    <div class="form-group">
      <input type="hidden" name="form[id]" id="idPers" value="<?=intval($item->id)?>">
      <div class="col-md-4">
        <label class="control-label">Фамилия: *</label>
        <input type="text" class="form-control" name="form[surname]" placeholder="Фамилия" 
               value="<?=htmlspecialchars($item->surname)?>">
      </div>
      <div class="col-md-4">
        <label class="control-label">Имя: *</label>
        <input type="text" class="form-control" name="form[name]" placeholder="Имя" 
               value="<?=htmlspecialchars($item->name)?>">
      </div>
      <div class="col-md-4">
        <label class="control-label">Отчество: *</label>        
        <input type="text" class="form-control" name="form[patronymic]" placeholder="Отчество"
               value="<?=htmlspecialchars($item->patronymic)?>">
      </div>
    </div>
    
    <div class="form-group">   
      <div class="col-md-4">
        <label class="control-label">Серия паспорта: *</label>
        <input type="text" class="form-control" name="form[passport]" placeholder="Серия паспорта"
               value="<?=htmlspecialchars($item->passport)?>">
      </div>
      <div class="col-md-4">
        <label class="control-label">Номер телефона:</label>
        <input type="text" class="form-control" name="form[phone]" placeholder="Номер телефона"
               value="<?=htmlspecialchars($item->phone)?>">
      </div>
      <div class="col-md-4">
        <label class="control-label">День рождения: *</label>
        <input type="text" class="form-control" id="datepicker" name="form[birthday]" placeholder="гггг-мм-дд"
               value="<?=htmlspecialchars($item->birthday)?>">
      </div>
    </div>
    
    <div class="form-group">    
      <div class="col-md-6">
        <label class="result" id="resultPers">Загрузка...</label>
      </div>
      <div class="col-md-6 text-right">
        <button type="submit" class="btn btn-info" id="createCli"><?=$item->id ? 'Изменить' : 'Добавить'?></button>
      </div>
    </div>
  </form>
</div>