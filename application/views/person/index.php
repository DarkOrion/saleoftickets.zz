<!--
Copyright (C) 2015 Maxim Ivanov (dark_orion)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!--

@author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
-->

<div class="row">
  <h1>Клиенты и пассажиры</h1>
</div>
<div class="row">
  <a href="/person/create" class="btn btn-info btn-md">Добавить персону</a>
</div>
<div class="row table-responsive">
  <table class="table table-bordered table-striped table-condensed">
    <tr class="info">
      <th>ID</th>
      <th>Фамилия</th>
      <th>Имя</th>
      <th>Отчество</th>
      <th>Пасспорт</th>
      <th>Телефон</th>
      <th>День рождения</th>
      <th>Ред</th>
      <th>Уд</th>
    </tr>
    <?php foreach ($items as $item) { ?>
        <tr>
          <td class="col-md-1"><?= $item->id ?></td>
          <td class="col-md-2"><?= $item->surname ?></td>
          <td class="col-md-2"><?= $item->name ?></td>
          <td class="col-md-2"><?= $item->patronymic ?></td>
          <td class="col-md-1"><?= $item->passport ?></td>
          <td class="col-md-1"><?= $item->phone ?></td>
          <td class="col-md-1"><?= $item->birthday ?></td>
          <td class="col-md-1"><a href="/person/update/<?= $item->id ?>" class="glyphicon glyphicon-pencil"></a></td>
          <td class="col-md-1"><a href="/person/delete/<?= $item->id ?>" class="glyphicon glyphicon-remove-circle"></a></td>
        </tr>
    <?php }; ?>
  </table>
</div>
