<!DOCTYPE html>
<!--
Copyright (C) 2015 Maxim Ivanov (dark_orion)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!--

@author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
-->

<div class="row">
  <h1>Купить билет</h1>
</div>

<div class="row col-sm-12 center-block form-row">

  <form class="form-horizontal" method="post">
    <div class="row">
      <h2 class="col-md-6">Поиска рейса</h2> 
    </div>
    <div class="form-group">   
      <div class="col-md-5">
        <label for="findDepart" class="control-label">Пункт отправления:</label>
        <input type="text" class="form-control" id="findDepart" name="form[dep]" placeholder="Город">
      </div>
      <div class="col-md-5 col-md-offset-2">
        <label for="findDest" class="control-label">Пункт назначения:</label>
        <input type="text" class="form-control" id="findDest" name="form[dest]" placeholder="Город">
      </div>  
    </div>
    <div class="form-group"> 
      <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-success" id="findFli">Найти</button>
      </div>
    </div>

    <table class="table">
      <thead>
        <tr>
          <th>Тур</th>
          <th>Вылет</th>
          <th>Прибытие</th>
          <th>Мест</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($items as $item) { ?>
        <tr>
         
          <td class="col-md-5"><?= $item->airp_depart ?> (<?= $item->city_depart ?>, 
             <?= $item->country_depart ?>) — <?= $item->airp_dest ?> (<?= $item->city_dest ?>,
             <?= $item->country_dest ?>) </td>
          <td class="col-md-2"><?= $item->depart_date ?></td>
          <td class="col-md-2"><?= $item->arrival_date ?></td>
          <td class="col-md-2"><?= $item->cl_av1 ?> <?= $item->cl_av2 ?> <?= $item->cl_av3 ?></td>
          <td class="col-md-1"><a href="/buy/flight/<?= $item->id ?>" class="btn btn-info btn-md">Заказать</a></td>
        </tr>
    <?php }; ?>
      </tbody>
    </table>

</div>