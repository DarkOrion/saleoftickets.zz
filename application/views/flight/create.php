
<!--
Copyright (C) 2015 Maxim Ivanov (dark_orion)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!--

@author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
-->
<div class="row">
  <h1>Рейсы</h1>
</div>
<a href="/flight/index" class="btn btn-info btn-md">Список рейсов</a>
<div class="row col-sm-12 center-block form-row">
  <h2><?= $item->id ? 'Изменить данные' : 'Добавить рейс' ?></h2> 
  <form class="form-horizontal" method="post" id="flightform">
    <div class="form-group">
      <input type="hidden" name="form[id]" id="idFli" value="<?= intval($item->id) ?>">
      <div class="col-md-7">
        <label class="control-label">Тур:</label>
        <select class="form-control"  name="form[id_voy]">
            <?php foreach ($voyages as $voy) { ?>
              <option value="<?= intval($voy->id) ?>">
                <?= htmlspecialchars($voy->airp_depart) ?> (<?= htmlspecialchars($voy->city_depart) ?>, 
                <?= htmlspecialchars($voy->country_depart) ?>)
                —  <?= htmlspecialchars($voy->airp_dest) ?> (<?= htmlspecialchars($voy->city_dest) ?>, 
                <?= htmlspecialchars($voy->country_dest) ?>)
              </option>
          <?php }; ?>
        </select>
      </div>
      <div class="col-md-5">
        <label class="control-label">Самолет: </label>
        <select class="form-control" name="form[id_airc]">
            <?php foreach ($aircrafts as $airc) { ?>
              <option value="<?= intval($airc->id) ?>">
                  <?= htmlspecialchars($airc->airc_name) ?> 
                (Мест: <?= htmlspecialchars($airc->seats_1d) ?>, <?= htmlspecialchars($airc->seats_2d) ?>, 
                <?= htmlspecialchars($airc->seats_3d) ?>; Топливо: <?= htmlspecialchars($airc->fuel) ?>)
              </option>
          <?php }; ?>
        </select>
      </div>
    </div>
    
    <div class="form-group">
      <div class="col-md-4">
        <label class="control-label">Время отправления: </label>
        <input type="text" class="form-control" id="datetimepicker1" name="form[depart_date]" placeholder="гггг-мм-дд чч:мм"
               value="<?=htmlspecialchars($item->depart_date)?>">
      </div>
      <div class="col-md-4">
        <label class="control-label">Время прибытия: </label>
        <input type="text" class="form-control" id="datetimepicker2" name="form[arrival_date]" placeholder="гггг-мм-дд чч:мм"
               value="<?=htmlspecialchars($item->arrival_date)?>">
      </div>
      <div class="col-md-4">
        <label class="control-label">Базовая стоимость: </label>
        <input type="text" class="form-control" name="form[base_cost]" placeholder="Стоимость в гривнах"
               value="<?=intval($item->base_cost)?>">
      </div>
    </div>
    
    <div class="form-group">    
      <div class="col-md-6">
        <label class="result" id="resultFli">Загрузка...</label>
      </div>
      <div class="col-md-6 text-right">
        <button type="submit" class="btn btn-info" id="createFli"><?=$item->id ? 'Изменить' : 'Добавить'?></button>
      </div>
    </div>
  </form>
</div>
