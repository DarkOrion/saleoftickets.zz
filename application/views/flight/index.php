<!--
Copyright (C) 2015 Maxim Ivanov (dark_orion)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!--

@author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
-->
<div class="row">
  <h1>Рейсы</h1>
</div>
<div class="row">
  <a href="/flight/create" class="btn btn-info btn-md">Добавить рейс</a>
</div>
<div class="row table-responsive">
  <table class="table table-bordered table-striped table-condensed">
    <tr class="info">
      <th>ID</th>
      <th>Отправление</th> 
      <th>Прибытие</th>
      <th>Самолет</th>
      <th>1-й класс</th>
      <th>2-й класс</th>
      <th>3-й класс</th>  
      <th>Ред.</th>
      <th>Уд.</th>
    </tr>
    <?php foreach ($items as $item) { ?>
        <tr>
          <td class="col-md-1"><?= $item->id ?></td>
          <td class="col-md-3"><?= $item->airp_depart ?> (<?= $item->city_depart ?>, 
             <?= $item->country_depart ?>)<br> <?= $item->depart_date ?></td>
          <td class="col-md-3"><?= $item->airp_dest ?> (<?= $item->city_dest ?>,
             <?= $item->country_dest ?>) <br> <?= $item->arrival_date ?></td>
          <td class="col-md-1"><?= $item->airc ?></td>
          <td class="col-md-1"><?= $item->cl_av1 ?></td>
          <td class="col-md-1"><?= $item->cl_av2 ?></td>
          <td class="col-md-1"><?= $item->cl_av3 ?></td>
          <td class="col-md-1"><a href="/flight/update/<?= $item->id ?>" class="glyphicon glyphicon-pencil"></a></td>
          <td class="col-md-1"><a href="/flight/delete/<?= $item->id ?>" class="glyphicon glyphicon-remove-circle"></a></td>
        </tr>
    <?php }; ?>
  </table>
</div>
