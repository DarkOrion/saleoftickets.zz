<!--
Copyright (C) 2015 Maxim Ivanov (dark_orion)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!--

@author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
-->

<article>
  <header>
    <nav class="navbar navbar-inverse row">
      <a href="/index/" class="navbar-brand">
        <img src="/assets/images/label_saleblue.png" alt="Sale off tickets" 
             width="30" height="30">  
      </a>
      <a href="/index/" class="navbar-brand backend-brand">Sale off tickets</a>
      <ul class="nav navbar-nav pull-right">
        <li><a href="/index/">Главная</a></li>
        <li><a href="/buy/">Купить билет</a></li>
        <li><a href="/about.html">О проекте</a></li>
      </ul>
    </nav><!-- /.navbar -->
  </header>


  <div class="row">
    <section class="col-md-9 content">
        <?= $content ?>
    </section><!-- /content -->

    <aside class="col-md-3">
      <form method="post">
        <div class="form-group">
          <label for="exampleInputEmail1">Логин</label>
          <input type="text" class="form-control" name="login" id="exampleInputLogin" placeholder="Введите логин">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Пароль</label>
          <input type="password" class="form-control" name="password" id="exampleInputPassword" placeholder="Password">
        </div>
        <button type="submit" formaction="/user/login/" class="btn btn-success btn-md">Войти</button>
      </form>
      <h2 class="text-center">Горячие туры</h2>      
      <nav>
          <?php extract(['items' => Flight::models()]); ?>
        <ul>
            <?php foreach ($items as $item) { ?>
              <li><a href="/buy/flight/<?= $item->id ?>"> <?= $item->city_depart ?>, 
                  <?= $item->country_depart ?> - <?= $item->city_dest ?>, <?= $item->country_dest ?><br></a>
                Вылет: <?= $item->arrival_date ?><br> 
                Прибытие: <?= $item->depart_date ?> 
              </li>
          <?php }; ?>
        </ul>
      </nav>
    </aside>
  </div>

  <footer class="row">
    <p class="col-md-6">Иванов М.А. КНТз-410</p>
    <p class="col-md-6 text-right">2015</p>
  </footer>
</article>

<?php $this->addStyleSheet('/assets/css/frontend.css', 'body'); ?>
<?php $this->addScript('/assets/js/frontend.js', 'body') ?>
<?php $this->addStyleSheet('/assets/css/jquery.datetimepicker.css', 'body'); ?>
<?php $this->addScript('/assets/js/jquery.datetimepicker.js', 'body') ?>
