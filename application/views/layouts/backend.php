<!--
 Copyright (C) 2015 Maxim Ivanov (dark_orion)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!--
 @author Maksim Ivanov (dark_orion) <minotaurmax@hotmail.com>
-->

<article>
  <header>
    <div class="navbar navbar-inverse">
      <a href="/index/" class="navbar-brand">
        <img src="/assets/images/label_saleblue.png" alt="Sale off tickets" 
             width="30" height="30">  
      </a>
      <a href="/index/" class="navbar-brand backend-brand">Sale off tickets</a>
      <ul class="nav navbar-nav pull-right">
        <li><a href="/admin/">Главная</a></li>
        <li><a href="/person/">Персоны</a></li>
        <li><a href="/flight/">Рейсы</a></li>
        <li><a href="/order/">Заказы</a></li>
        <li><a href="/admin/exit">Выйти</a></li>
      </ul>
    </div><!-- /.navbar -->
  </header>

  <section class="row content">
      <?= $content ?>
  </section><!-- /.row content -->

  <footer class="row">
    <p class="col-md-6">Иванов М.А. КНТз-410</p>
    <p class="col-md-6 text-right">2015</p>
  </footer>
</article>

<?php $this->addStyleSheet('/assets/css/backend.css', 'body'); ?>
<?php $this->addScript('/assets/js/backend.js', 'body') ?>
<?php $this->addStyleSheet('/assets/css/jquery.datetimepicker.css', 'body'); ?>
<?php $this->addScript('/assets/js/jquery.datetimepicker.js', 'body') ?>