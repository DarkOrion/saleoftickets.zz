<?php

/*
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of User
 * 
 * @author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */
class User extends Model
{
    
    public $safe = ['id', 'login', 'password', 'email'];

    function isAdmin()
    {
        if (isset($_SESSION['auth']) && $_SESSION['auth']) {
            return true;
        }
        $_SESSION['auth'] = ($this->login == 'admin' and $this->password == 'admin') ? true : false;
        return $_SESSION['auth'];
    }

}
