<?php

/*
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of Voyge
 * 
 * @author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */
class Voyage extends ModelTable
{
    static public $table = 'voyages';
    public $safe = ['id', 'id_depart', 'id_dest', 'airp_dest', 'airp_depart', 
        'city_dest', 'city_depart', 'country_dest', 'country_depart'];
    
    static function getQuery()
    {
        $modelname = get_called_class();
        return App::gi()->db->query(
                  'SELECT voyages.*, '
                .   'dep.name_airp AS airp_depart, '
                .   'des.name_airp AS airp_dest, '
                .   'des.city AS city_dest,'
                .   'dep.city AS city_depart, '
                .   'des.country AS country_dest,'
                .   'dep.country AS country_depart '
                . 'FROM ?n '
                .   'INNER JOIN ?n dep ON voyages.id_depart = dep.id '
                .   'INNER JOIN ?n des ON voyages.id_dest = des.id '
                , $modelname::$table, 'airports', 'airports');
    }
    
    public function beforeSave()
    {
        return parent::beforeSave();
    }
}
