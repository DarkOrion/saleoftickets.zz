<?php

/*
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of Ticket
 * 
 * @author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */
class Ticket extends ModelTable
{

    static public $table = 'book_tickets';
    public $safe = ['id', 'id_fli', 'id_class', 'tot_value', 'id_pass',
        'city_dest', 'city_depart', 'country_dest', 'country_depart', 'depart_date',
        'surname', 'passport', 'name', 'patronymic'];
    
    static function getQuery()
    {
        $modelname = get_called_class();
        return App::gi()->db->query(
                  'SELECT book_tickets.*, '
                .   'persons.surname AS surname, '
                .   'persons.name AS name, '
                .   'persons.patronymic AS patronymic, '
                .   'persons.passport AS passport, '
                .   'flights.depart_date AS depart_date, '
                .   'des.city AS city_dest,'
                .   'dep.city AS city_depart, '
                .   'des.country AS country_dest,'
                .   'dep.country AS country_depart '
                . 'FROM ?n '
                .   'INNER JOIN ?n ON book_tickets.id_pass = persons.id '
                .   'INNER JOIN ?n ON book_tickets.id_fli = flights.id '
                .   'INNER JOIN ?n ON flights.id_voy = voyages.id '
                .   'INNER JOIN ?n dep ON voyages.id_depart = dep.id '
                .   'INNER JOIN ?n des ON voyages.id_dest = des.id '
                , $modelname::$table, 'persons', 'flights', 'voyages', 'airports', 'airports');
    }

    public function beforeSave()
    {
        return parent::beforeSave();
    }

}
