<?php

/*
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of Flight
 * 
 * @author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */
class Flight extends ModelTable
{
    static public $table = 'flights';
    public $safe = ['id', 'airc', 'id_airc', 'id_voy', 'id_depart', 'id_dest', 'airp_dest', 
        'airp_depart', 'city_dest', 'city_depart', 'country_dest', 'country_depart',
        'cl_av1', 'cl_av2', 'cl_av3', 'base_cost', 'depart_date', 'arrival_date'];
    
    static function getQuery()
    {
        $modelname = get_called_class();
        return App::gi()->db->query(
                  'SELECT flights.*, '
                .   'aircrafts.airc_name AS airc, '
                .   'dep.name_airp AS airp_depart, '
                .   'des.name_airp AS airp_dest, '
                .   'des.city AS city_dest,'
                .   'dep.city AS city_depart, '
                .   'des.country AS country_dest,'
                .   'dep.country AS country_depart '
                . 'FROM ?n '
                .   'INNER JOIN ?n ON flights.id_airc = aircrafts.id '
                .   'INNER JOIN ?n ON flights.id_voy = voyages.id '
                .   'INNER JOIN ?n dep ON voyages.id_depart = dep.id '
                .   'INNER JOIN ?n des ON voyages.id_dest = des.id '
                , $modelname::$table, 'aircrafts', 'voyages', 'airports', 'airports');
    }
    
    public function beforeSave()
    {
        return parent::beforeSave();
    }
}
