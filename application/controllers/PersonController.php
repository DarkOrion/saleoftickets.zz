<?php

/*
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of PersonController
 * 
 * @author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */
class PersonController extends AdminController
{
   
    function actionIndex() 
    {
        $user = new User();
        if (!$user->isAdmin()) {
            header('Location:/');
            exit();
        }
        $this->render('index',array('items'=>Person::models()));
    }
    
    function actionCreate($aj)
    {     
        $user = new User();
        if (!$user->isAdmin()) {
            header('Location:/');
            exit();
        }
        if (isset($aj)) {
            $person = new Person();
            if (isset($_POST['form'])) {
                $post = Model::filter($_POST['form']);
                $person->__attributes = $post;
                if ($person->save()) {
                    die("Персона добавлена");
                }
                else {
                    die("Ошибка добавления");
                }
            }
        }
        $this->render("create", array("item"=>$person));
    }
    
    function actionUpdate($id)
    {
        $user = new User();
        if (!$user->isAdmin()) {
            header('Location:/');
            exit();
        }
        $id = (int) $id ? (int) $id : (int) $_POST['form']['id'];
        $person = Person::model($id);
        if ($person->id) {
            if (isset($_POST['form'])) {
                $post = Model::filter($_POST['form']);
                $person->__attributes = $post;
                if ($person->save()) {
                    header('location:/person/index');
                    exit();
                }
            }
            $this->render('create', array('item' => $person));
        } else {
            throw new Except('Запись не найдена');
        }
    }
    
    function actionDelete($id)
    {
        $user = new User();
        if (!$user->isAdmin()) {
            header('Location:/');
            exit();
        }
        $id = (int) $id ? (int) $id : (int) $_POST['form']['id'];
        $person = Person::model($id);
        if (!$person->delete($id)) {
            exit('Ошибка удаления персоны');
        }
        $this->render('index',array('items'=>Person::models()));
    }
}
