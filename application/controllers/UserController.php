<?php

/*
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of UserController
 * 
 * @author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */
class UserController extends Controller
{

    function actionLogin()
    {
        $user = new User();
        if (isset($_POST['login'])) {
            $user->__attributes = Model::filter($_POST);
            if ($user->isAdmin()) {
                header('Location:/admin');
            } else {
                $this->error = 'Не верный пользователь или пароль';
            }
        }
        
    }

}
