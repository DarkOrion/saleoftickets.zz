<?php

/*
 * Copyright (C) 2015 Maxim Ivanov (dark_orion)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of FlightController
 * 
 * @author Maxim Ivanov (dark_orion) <minotaurmax@hotmail.com>
 */
class FlightController extends AdminController
{
   
    function actionIndex() 
    {
        $user = new User();
        if (!$user->isAdmin()) {
            header('Location:/');
            exit();
        }
        $this->render('index',['items'=>Flight::models()]);
    }
    
    function actionCreate($aj)
    {
        $user = new User();
        if (!$user->isAdmin()) {
            header('Location:/');
            exit();
        }
        if (isset($aj)) {
            $flight = new Flight();
            if (isset($_POST['form'])) {
                $post = Model::filter($_POST['form']);
                $flight->__attributes = $post;
                if ($flight->save()) {
                    die("Рейс добавлен");
                }
                else {
                    die("Ошибка добавления");
                }
            }
        }
        $this->render("create", ["item"=>$flight, 'voyages'=>Voyage::models(), 
            'aircrafts'=> Aircraft::models()]);
    }
    
    function actionDelete($id)
    {
        $user = new User();
        if (!$user->isAdmin()) {
            header('Location:/');
            exit();
        }
        $id = (int) $id ? (int) $id : (int) $_POST['form']['id'];
        $flight = Flight::model($id);
        if (!$flight->delete($id)) {
            exit('Ошибка удаления персоны');
        }
        $this->render('index',array('items'=>Flight::models()));
    }
}
